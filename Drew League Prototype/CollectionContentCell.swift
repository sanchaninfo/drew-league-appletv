//
//  CollectionContentCell.swift
//  AEOM
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import SystemConfiguration
import Kingfisher


class CollectionContentCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @objc var menuCollectionList = NSMutableArray()
   
    @objc var UserId,DeviceId,uuid : String!
    @objc var isMyList = Bool()
    @objc var CarousalDict = [[String:Any]]()
    @objc var isPhotos = Bool()
    @objc var isstore = Bool()
    @objc var userData = [[String:Any]]()
    @objc var activityView = UIView()

  
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuCollectionList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCell", for: indexPath)
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
        let img = (cell.viewWithTag(11) as! UIImageView)
        img.kf.indicatorType = .activity
        if isPhotos == true
        {
            (cell.viewWithTag(12) as! UIImageView).isHidden = true
            (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: (kPhotoBaseUrl + (Path["thumb"] as! String))))
            
        }
        else if isstore == true
        {
            (cell.viewWithTag(12) as! UIImageView).isHidden = true
            (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName: "StoreIcon")
        }
        else
        {
            if Path[kMetadata] != nil
            {
                let metaDataPath = Path[kMetadata] as! NSDictionary
                    if metaDataPath[kIsmenu] != nil
                    {
                        if metaDataPath[kIsmenu] as! Bool == true
                        {
                            (cell.viewWithTag(11) as! UIImageView).image = metaDataPath[kMovieart] as? UIImage
                        }
                    }
            }
            else
            {
                let img = (cell.viewWithTag(11) as! UIImageView)
                img.kf.indicatorType = .activity
                
                if (Path.object(forKey: kMovieart) is NSNull)
                {
                    (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName:"RevoltFlat")
                    
                }
                else
                {
                    if Path[kMovieart] != nil
                    {
                        if (Path[kMovieart] as! String == "")
                        {
                            // (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: metaDataPath[kMovieart] as! String))
                            (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName:"RevoltFlat")
                        }
                        else
                        {
                            (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: Path[kMovieart] as! String))
                        }
                    }
                    else
                        
                    {
                        (cell.viewWithTag(11) as! UIImageView).image = UIImage(imageLiteralResourceName:"RevoltFlat")
                    }
                    
                }
                if (Path["tve_provider_authorization"] != nil)
                {
                    
                    if (Path["tve_provider_authorization"] is NSString)
                    {
                        
                        if (Path["tve_provider_authorization"] as! NSString == "true")
                        {
                            if userData.count != 0
                            {
                                let data = userData.first! as NSDictionary
                                let provider = data["provider"] as! String
                                if provider == "" || provider == " "
                                {
                                    (cell.viewWithTag(12) as! UIImageView).isHidden = false
                                }
                                else
                                {
                                    (cell.viewWithTag(12) as! UIImageView).isHidden = true
                                }
                            }
                            
                        }
                        else
                        {
                            (cell.viewWithTag(12) as! UIImageView).isHidden = true
                        }
                    }
                    if (Path["tve_provider_authorization"] is Bool)
                    {
                        if (Path["tve_provider_authorization"] as! Bool == true)
                        {
                            if userData.count != 0
                            {
                                let data = userData.first! as NSDictionary
                                let provider = data["provider"] as! String
                                if provider == "" || provider == " "
                                {
                                    (cell.viewWithTag(12) as! UIImageView).isHidden = false
                                }
                                else
                                {
                                    (cell.viewWithTag(12) as! UIImageView).isHidden = true
                                }
                            }
                        }
                        else
                        {
                            (cell.viewWithTag(12) as! UIImageView).isHidden = true
                        }
                    }
                    
                }
                else
                {
                    (cell.viewWithTag(12) as! UIImageView).isHidden = true
                }
                
            }
        }
        collectionView.isScrollEnabled = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        let tableview:UITableView = self.superview?.superview as! UITableView
        //Previous Index Focus
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            cell.transform = .identity
        }
        
        //Next Index Focus
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath)
        {
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
            (cell.viewWithTag(11) as! UIImageView).adjustsImageWhenAncestorFocused = true
            let viewcontroller = tableview.dataSource as! MainViewController
    
            if (menuCollectionList.count > indexPath.row)
            {
                let Path = menuCollectionList[indexPath.row] as! NSDictionary
                if isPhotos == true
                {
                    viewcontroller.staticLbl.text = ""
                    viewcontroller.lbl2.text = ""
                    viewcontroller.MainImage.kf.indicatorType = .activity
                    viewcontroller.MainImage.kf.setImage(with: URL(string: (kPhotoBaseUrl + (Path["large"] as! String))))
                    viewcontroller.ReleaseDate.text = ""
                    //                    let carousel = Path["carousels"] as! NSArray
                    //                    let data = carousel.firstObject as! NSDictionary
                    viewcontroller.AssestName.text = (Path["title"] as! String)
                    viewcontroller.Description.text = ""
                    viewcontroller.TimeLbl.text = ""
                }
                else if isstore == true
                {
                    viewcontroller.staticLbl.text = ""
                    viewcontroller.lbl2.text = ""
                    viewcontroller.MainImage.kf.indicatorType = .activity
                    viewcontroller.MainImage.kf.setImage(with: URL(string: (Path["posterURL"] as! String)))
                    viewcontroller.ReleaseDate.text = ""
                    let carousel = Path["carousels"] as! NSArray
                    let data = carousel.firstObject as! NSDictionary
                    viewcontroller.AssestName.text = (data["carousel"] as! String)
                    viewcontroller.Description.text = ""
                    viewcontroller.TimeLbl.text = (Path["duration"] as! String)
                    (viewcontroller.view.viewWithTag(1))?.isHidden = false
                    (viewcontroller.cosmosview.isHidden) = true
                    viewcontroller.ratingLbl.isHidden = true
                    
                }
                else
                {
                    
                    if Path[kMetadata] != nil
                    {
                    let image = Path[kMetadata] as! NSDictionary
                    if image[kCarouselId] as! String == "Menu"
                    {
                        if indexPath.row == 0
                        {
                            viewcontroller.MainImage.kf.indicatorType = .activity
                            viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "Search_shelf")
                            viewcontroller.staticLbl.text = "Search"
                            viewcontroller.lbl2.text = "Search for Highlights, Player Interviews...."
                        }
                        if indexPath.row == 1
                        {
                            viewcontroller.MainImage.kf.indicatorType = .activity
                            viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "categories_shelf")
                            viewcontroller.staticLbl.text = "Categories"
                            viewcontroller.lbl2.text = ""
                            
                        }
                        if indexPath.row == 2
                        {
                            viewcontroller.MainImage.kf.indicatorType = .activity
                            viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "settings_Shelf")
                            viewcontroller.staticLbl.text = "Settings"
                            viewcontroller.lbl2.text = "Your account information"
                            
                        }
                        viewcontroller.AssestName.text = ""
                        viewcontroller.ReleaseDate.text = ""
                        viewcontroller.TimeLbl.text = ""
                        viewcontroller.Description.text = ""
                        viewcontroller.ratingLbl.isHidden = true
                        viewcontroller.ratingLbl.text = ""
                        (viewcontroller.view.viewWithTag(1))?.isHidden = true
                        (viewcontroller.cosmosview.isHidden) = true
                    }
                    }
                    else
                    {
                       // banner_image_1300x650
                         getstarRating(assetId:Path["assetId"] as! String,UserId:self.UserId)
                        var tmpValue = String()
                        viewcontroller.staticLbl.text = ""
                        viewcontroller.lbl2.text = ""
                        viewcontroller.MainImage.kf.indicatorType = .activity
                        if (isnil(json: Path, key: "banner_image_1300x650") == " ") || (isnil(json: Path, key: "banner_image_1300x650") == "")
                        {
                            viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "bannerImg")
                        }
                        else
                        {
                             viewcontroller.MainImage.kf.setImage(with: URL(string: isnil(json: Path, key: "banner_image_1300x650")))
                        }
                        let name = (isnil(json: Path, key: "assetName")).capitalized
                        let maxLength = 40
                        if name.count > maxLength {
                            let range =  name.rangeOfComposedCharacterSequences(for: name.startIndex..<name.index(name.startIndex, offsetBy: maxLength))
                         //   tmpValue = name.substring(with: range).appending("...")
                            tmpValue = name[range].appending("...")
                        }
                        else
                        {
                            tmpValue = name
                        }
                    
                        viewcontroller.AssestName.text = tmpValue
                        let labelText = tmpValue
                        let lbl = viewcontroller.AssestName
                        lbl?.frame = CGRect(x: (lbl?.frame.origin.x)!, y: (lbl?.frame.origin.y)!, width: (labelText.widthWithConstrainedWidth(height: 60, font: (lbl?.font)!)), height: (lbl?.frame.size.height)!)
                        
                        let str1 = (isnil(json: Path, key: "release_date")).components(separatedBy: "-")
                        viewcontroller.ReleaseDate.text = str1[0]
                        
                        // viewcontroller.TimeLbl.text =  stringFromTimeInterval(interval: Double(isnil(json: Path, key: "file_duration"))!)
                      
                        let time = isnil(json:Path,key:"duration")
                        let time1 = Double(time)
                        if time1 != nil
                        {
                            viewcontroller.TimeLbl.text =  stringFromTimeInterval(interval: time1!)
                        }
                        else
                        {
                            viewcontroller.TimeLbl.text = ""
                        }
                       
                        let DescriptionText = isnil(json: Path, key: "description")
                        let destxt = DescriptionText.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
                        let destxt1 =  destxt.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
                        let destxt2 = destxt1.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
                        
                        viewcontroller.Description.text = destxt2
                        if (DescriptionText.contains("n/a"))
                        {
                            viewcontroller.Description.text = ""
                        }
                        let desLbl = viewcontroller.Description
                        desLbl?.frame = CGRect(x: (desLbl?.frame.origin.x)!, y: (desLbl?.frame.origin.y)!, width: (desLbl?.frame.size.width)!, height: (DescriptionText.widthWithConstrainedWidth(height: 60, font: (desLbl?.font)!)))
                        (viewcontroller.view.viewWithTag(1))?.isHidden = false
                        (viewcontroller.cosmosview.isHidden) = false
                        viewcontroller.ratingLbl.isHidden = false
                    }
                    
                }

            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
        if isPhotos == true
        {
            
            let tableview:UITableView = self.superview?.superview as! UITableView
            let viewcontroller = tableview.dataSource as! MainViewController
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let PhotoPage = storyBoard.instantiateViewController(withIdentifier: "Photos") as! PhotosViewController
            PhotoPage.PhotoDict = viewcontroller.PhotoDict
            PhotoPage.ImageIndexpath = indexPath.row
            viewcontroller.navigationController?.pushViewController(PhotoPage, animated: true)
        }
        else if isstore == true
        {
            let tableview:UITableView = self.superview?.superview as! UITableView
            let viewcontroller = tableview.dataSource as! MainViewController
            let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
            let StorePage = storyBoard.instantiateViewController(withIdentifier: "Store") as! StoreViewController
            StorePage.prodData = Path
            StorePage.userId = UserId
            StorePage.uuid = uuid
            StorePage.deviceID = DeviceId
            viewcontroller.navigationController?.pushViewController(StorePage, animated: true)
        }
        else
        {
            if Path[kMetadata] != nil
            {
                let MetaDict = Path[kMetadata] as! NSDictionary
                if ((MetaDict[kCarouselId] as! String) == "Menu")
                {
                   
                    if indexPath.row == 0
                    {
                        let tableview:UITableView = self.superview?.superview as! UITableView
                        let viewcontroller = tableview.dataSource as! MainViewController
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        guard let searchResultsController = storyboard.instantiateViewController(withIdentifier: "Search") as? SearchListViewController
                            else {
                                fatalError("Unable to instatiate a SearchResultsViewController from the storyboard.")
                        }
                        searchResultsController.searchCollectionList = viewcontroller.searchCollectionList
                        searchResultsController.userId = viewcontroller.userId
                        searchResultsController.deviceId = viewcontroller.deviceId
                        searchResultsController.uuid = viewcontroller.uuid
                        searchResultsController.storeProdData = viewcontroller.storeProdData
                        searchResultsController.Donatedict = viewcontroller.DonateDict
                        searchResultsController.searchDelegate = viewcontroller
                        searchResultsController.userData = self.userData
                        viewcontroller.navigationController!.pushViewController(searchResultsController, animated: false)
                        
                    }
                    if indexPath.row == 1
                    {
                        gotoCategories()
                    }
                    if indexPath.row == 2
                    {
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let SettingsPage = storyBoard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                        let tableview:UITableView = self.superview?.superview as! UITableView
                        let viewcontroller = tableview.dataSource as! MainViewController
                        SettingsPage.deviceId = viewcontroller.deviceId
                        SettingsPage.uuid = viewcontroller.uuid
                        SettingsPage.userId = viewcontroller.userId
                        viewcontroller.navigationController?.pushViewController(SettingsPage, animated: true)
                        
                    }
                }
            }
            else
            {
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                activityView = ActivityView.init(frame:(viewcontroller.view.frame))
                viewcontroller.view.addSubview(activityView)
                viewcontroller.listView.isUserInteractionEnabled = false
                
                getaccountInfo(id:Path["assetId"] as! String,userid: UserId/*,tvshow: (Path["tv_show"] as! Bool)*/)
                
                
               // self.getAssetData(withUrl: kAssestDataUrl, id: Path["assetId"] as! String, userid: self.UserId, subscription_status: "false",carousal_id: "")
                //   getAssetData(withUrl:kAssestDataUrl,id: Path["id"] as! String,userid: UserId)
            }
        }
      }
    
   /* func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell in self.menuCollectionView.visibleCells {
            let indexPath = self.menuCollectionView.indexPath(for: cell)!
            let attributes = self.menuCollectionView.layoutAttributesForItem(at: indexPath)!
            let cellRect = attributes.frame
            let cellFrameInSuperview = self.menuCollectionView.convert(cellRect, to: self.menuCollectionView.superview)
            let centerOffset = cellFrameInSuperview.origin.x - self.menuCollectionView.contentInset.left
            
            cell.updateWithOffset(centerOffset)
        }
    }*/
    @objc func carousalSelected1(carousalName:String,carousalDetailDict:[NSDictionary],userid:String,deviceid:String,uuid:String,userData:[[String:Any]])
    {
        self.menuCollectionList.removeAllObjects()
        self.UserId = userid
        self.DeviceId = deviceid
        // self.CarousalDict = carousalDetailDict
        self.uuid = uuid
        self.userData = userData
        
        if carousalName != "Menu" || carousalName != "My List" || carousalName != "Recently Watched" || carousalName != "Store" || carousalName != "Photos" || carousalName != "SpotLight"
        {
            let name = carousalName.replacingOccurrences(of: "%20", with: " ")
            self.menuCollectionList.removeAllObjects()
            
            for i in 0..<carousalDetailDict.count
            {
                if (carousalDetailDict[i][name]) != nil
                {
                    let arr = (((carousalDetailDict[i])[name]) as! [[String:Any]])
                    for dict in arr
                    {
                        let dicton = dict as NSDictionary
                        self.menuCollectionList.add(dicton)
                    }
                }
                
            }
            isstore = false
            isPhotos = false
            
        }
        else
        {
            for dict in carousalDetailDict
            {
                if carousalName == "Menu"
                {
                    self.menuCollectionList.add(dict)
                }
                else
                {
                    if carousalName == "My List"
                    {
                        if dict[kData] != nil
                        {
                            let dicton = dict[kData] as! NSDictionary
                            self.menuCollectionList.add(dicton)
                            self.isMyList = true
                            isstore = false
                            isPhotos = false
                        }
                    }
                    else if carousalName == "Recently Watched"
                    {
                        if dict[kData] != nil
                        {
                            let dicton = dict[kData] as! NSDictionary
                            self.menuCollectionList.add(dicton)
                            isstore = false
                            isPhotos = false
                        }
                        //                    else
                        //                    {
                        //                        self.menuCollectionList.add(dict)
                        //                    }
                    }
                    else if carousalName == "Store"
                    {
                        isPhotos = false
                        isstore = true
                        self.menuCollectionList.add(dict)
                        //  self.menuCollectionList.add(CarousalDict)
                        
                    }
                    else if carousalName == "Photos"
                    {
                        isPhotos = true
                        isstore = false
                        self.menuCollectionList.add(dict)
                    }
                    else if carousalName == "SpotLight"
                    {
                        isstore = false
                        isPhotos = false
                        
                        self.menuCollectionList.add(dict)
                    }
                        //                else if carousalName == "Recently Added"
                        //                {
                        //                    isstore = false
                        //                    isPhotos = false
                        //
                        //                    self.menuCollectionList.add(dict)
                        //                }
                        
                    else
                    {
                        
                        if dict[kCarouselId] is String
                        {
                            if (dict[kCarouselId] as! String) == carousalName
                            {
                                self.menuCollectionList.add(dict)
                                isstore = false
                                isPhotos = false
                            }
                        }
                        if dict[kCarouselId] is NSArray
                        {
                            if (((dict[kCarouselId] as! NSArray).firstObject)as! String) == carousalName
                            {
                                self.menuCollectionList.add(dict)
                                isstore = false
                                isPhotos = false
                            }
                        }
                        //                    if dict[kMetadata] != nil
                        //                    {
                        //                        let metaDict = dict[kMetadata] as! NSDictionary
                        //                        if (metaDict[kCarouselId] as! String) == carousalName
                        //                        {
                        //                            self.menuCollectionList.add(dict)
                        //                        }
                        //                    }
                        
                        /*
                         if kMetadata != ""
                         {
                         let metaDict = dict[kMetadata] as! NSDictionary
                         if (metaDict[kCarouselId] as! String) == carousalName
                         {
                         self.menuCollectionList.add(dict)
                         }
                         
                         }
                         else
                         {
                         let metaDict = dict[kMetadata] as! NSDictionary
                         if (metaDict[kCarouselId] as! String) == carousalName
                         {
                         self.menuCollectionList.add(dict)
                         }
                         
                         }*/
                        
                    }
                }
            }
        }
        
        
        DispatchQueue.main.async {
            self.menuCollectionView.isHidden = false
            //   activityView.removeFromSuperview()
            // self.menuCollectionView.isUserInteractionEnabled = true
            self.menuCollectionView.reloadData()
            
        }
    }
    
    @objc func carousalSelected(carousalName:String,carousalDetailDict:[[String:Any]],userid:String,deviceid:String,uuid:String,userData:[[String:Any]])
    {
        self.menuCollectionList.removeAllObjects()
        self.UserId = userid
        self.DeviceId = deviceid
        self.CarousalDict = carousalDetailDict
        self.uuid = uuid
        self.userData = userData
        for dict in carousalDetailDict
        {
            if carousalName == "Menu"
            {
                self.menuCollectionList.add(dict)
            }
            else
            {
                if carousalName == "My List"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                        self.isMyList = true
                        isstore = false
                        isPhotos = false
                    }
                }
                else if carousalName == "Recently Watched"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                        isstore = false
                        isPhotos = false
                    }
//                    else
//                    {
//                        self.menuCollectionList.add(dict)
//                    }
                }
                else if carousalName == "Store"
                {
                    isPhotos = false
                    isstore = true
                    self.menuCollectionList.add(dict)
                    //  self.menuCollectionList.add(CarousalDict)
                    
                }
                else if carousalName == "Photos"
                {
                    isPhotos = true
                    isstore = false
                    self.menuCollectionList.add(dict)
                }
                else if carousalName == "SpotLight"
                {
                    isstore = false
                    isPhotos = false
                    
                    self.menuCollectionList.add(dict)
                }
                else
                {
                 
                    if dict[kCarouselId] is String
                    {
                        if (dict[kCarouselId] as! String) == carousalName
                        {
                           self.menuCollectionList.add(dict)
                            isstore = false
                            isPhotos = false
                        }
                    }
                    if dict[kCarouselId] is NSArray
                    {
                        if (((dict[kCarouselId] as! NSArray).firstObject)as! String) == carousalName
                        {
                           self.menuCollectionList.add(dict)
                            isstore = false
                            isPhotos = false
                        }
                    }
//                    if dict[kMetadata] != nil
//                    {
//                        let metaDict = dict[kMetadata] as! NSDictionary
//                        if (metaDict[kCarouselId] as! String) == carousalName
//                        {
//                            self.menuCollectionList.add(dict)
//                        }
//                    }
                    
                /*
                    if kMetadata != ""
                    {
                        let metaDict = dict[kMetadata] as! NSDictionary
                        if (metaDict[kCarouselId] as! String) == carousalName
                        {
                            self.menuCollectionList.add(dict)
                        }

                    }
                    else
                    {
                        let metaDict = dict[kMetadata] as! NSDictionary
                        if (metaDict[kCarouselId] as! String) == carousalName
                        {
                            self.menuCollectionList.add(dict)
                        }

                    }*/
                  
                }
            }
        }
        DispatchQueue.main.async {
            self.menuCollectionView.isHidden = false
         //   activityView.removeFromSuperview()
           // self.menuCollectionView.isUserInteractionEnabled = true
            self.menuCollectionView.reloadData()
            
        }
    }
    
    // Service Call for getAssetData
    @objc func getAssetData(withUrl:String,id:String,userid:String,/*tvshow:Bool,*/subscription_status:String,carousal_id:String)
    {
        //        if accountstatus == true
        //        {
        var parameters =  [String:[String:AnyObject]]()
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MainViewController
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let osVersion = UIDevice.current.systemVersion
        let className = NSStringFromClass(self.classForCoder)

        var user_ID = String()
        if userid == ""
        {
            user_ID = ""
        }
        else
        {
            user_ID = userid
        }
        parameters = ["getAssetData":["videoId":id as AnyObject,"userId":user_ID as AnyObject]]
        let withUrl = kAssestDataUrl + "appname=\(kAppName)&assetId=\(id)&token=\(user_ID)"
         
        let lambdaparameters = ["appname":"\(kAppName)"]
        
        ApiManager.sharedManager.postDataWithJsonLambda(url: withUrl, parameters:lambdaparameters){
            (responseDict,error,isDone)in
            if error == nil
            {
                
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.DeviceId
                DetailPage.uuid = self.uuid
                DetailPage.storeProdData = viewcontroller.storeProdData
                DetailPage.Donatedict = viewcontroller.DonateDict
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                DetailPage.delegate = viewcontroller
                DetailPage.fromMyList = self.isMyList
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    DetailPage.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                    let dict = responseDict as! NSDictionary
                
                    if dict["statusCode"] != nil
                    {
                        let status = dict["statusCode"] as! Int
                        if status == 200
                        {
                           let result = dict["result"] as! NSDictionary
                           DetailPage.TvshowPath = result
                           viewcontroller.navigationController?.pushViewController(DetailPage, animated: true)
                        }
                        else
                        {
                            if status == 401
                            {
                                let error = dict["error"] as! String
                                if error == "Invalid user"
                                {
                                    self.pairingPage()
                                }
                            }
                        }
                    }
                    else
                    {
                     //   print("json error")
                        flixlog(str: "json error")
                    }
                }
                
            }
                
            else
            {
                let listError = (error?.localizedDescription)! as String
                UserDefaults.standard.set(listError, forKey: "locerror")
                UserDefaults.standard.synchronize()
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    let _ = viewcontroller.navigationController?.popToRootViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                // viewcontroller.navigationController?.pushViewController(alertview, animated: true)
                viewcontroller.present(alertview, animated: true, completion: nil)
                parameters =  ["insertLog":["source":("\(withUrl) - \(className) - \(#function)") as AnyObject,"error":(listError) as AnyObject,"code":parameters as AnyObject,"details":("userId:\(userid) - AppVersion:\(appVersionString) - OSVersion:\(osVersion)") as AnyObject,"device":"AppleTV" as AnyObject]]
//                ApiManager.sharedManager.postDataWithJson(url: kLogUrl, parameters: parameters)
//                {(responseDict,error,isDone)in
//                    if error == nil
//                    {
//                        _ = responseDict
//                    }
//                }
            }
            viewcontroller.listView.isUserInteractionEnabled = true
            self.activityView.removeFromSuperview()
        }
        
        
      /*
        ApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                
                let JSON = responseDict
              
                let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                if JSON is NSArray
                {
                    let dict = JSON as! NSArray
                    DetailPage.TvshowPath = dict.firstObject as! NSDictionary
                }
                else
                {
                   DetailPage.TvshowPath = JSON as! NSDictionary
                }
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.DeviceId
                DetailPage.uuid = self.uuid
                DetailPage.storeProdData = viewcontroller.storeProdData
                DetailPage.Donatedict = viewcontroller.DonateDict
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                DetailPage.delegate = viewcontroller
                DetailPage.fromMyList = self.isMyList
                viewcontroller.navigationController?.pushViewController(DetailPage, animated: true)
            }
                
            else
            {
                let listError = (error?.localizedDescription)! as String
                UserDefaults.standard.set(listError, forKey: "locerror")
                UserDefaults.standard.synchronize()
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                let _ = viewcontroller.navigationController?.popToRootViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                // viewcontroller.navigationController?.pushViewController(alertview, animated: true)
                viewcontroller.present(alertview, animated: true, completion: nil)
                parameters =  ["insertLog":["source":("\(withUrl) - \(className) - \(#function)") as AnyObject,"error":(listError) as AnyObject,"code":parameters as AnyObject,"details":("userId:\(userid) - AppVersion:\(appVersionString) - OSVersion:\(osVersion)") as AnyObject,"device":"AppleTV" as AnyObject]]
                ApiManager.sharedManager.postDataWithJson(url: kLogUrl, parameters: parameters)
                {(responseDict,error,isDone)in
                    if error == nil
                    {
                        _ = responseDict
                    }
                }
            }
        }*/
    }
    
    @objc func getaccountInfo(id:String,userid:String/*tvshow:Bool*/)
    {
      //  var parameters =  [String:[String:AnyObject]]()
      //  parameters = ["getAccountInfo":["deviceId":DeviceId as AnyObject,"uuid":uuid as AnyObject]]
     //   let url = kAccountInfoUrl + "appname=\(kAppName)&device_id=\(DeviceId!)&uuid=\(uuid!)"
        let url = kAccountInfoUrl + "appname=\(kAppName)&token=\(self.UserId!)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
               let json = responseDict as! NSDictionary
               if json["statusCode"] != nil
               {
                let status = json["statusCode"] as! Int
                if status == 200
                {
                   let result = json["result"] as! NSDictionary
                    accountresponse = result["uuid_exist"] as! Bool
                    if accountresponse == true
                    {
                        self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid,/*tvshow:tvshow,*/subscription_status:(result.value(forKey: "subscription_status") as! String), carousal_id: "")
                    }
                    else
                    {
                       self.pairingPage()
                    }
                }
                    
                else
                {
                   if status == 401
                   {
                       self.pairingPage()
                   }
                }
               }
               else
               {
                 // print("json error")
                flixlog(str: "json error")
               }
                
            }
            else
            {
             //   print(error?.localizedDescription ?? "getaccountInfo got with error")
                flixlog(str:error?.localizedDescription ?? "getaccountInfo got with error" )
            }
        }
        
       // let url = "http://34.200.110.244:9000/getAccountInfo"
     /*   ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters){
            (responseDict,error,isDone)
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
             
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == true
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid,/*tvshow:tvshow,*/subscription_status:(dict.value(forKey: "subscription_status") as! String), carousal_id: "")
                    
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }*/
        
    }
    
    func pairingPage()
    {
        let url = kActivationCodeUrl + "appname=\(kAppName)&model=AppleTV4Gen&manufacturer=Apple&device_name=AppleTV&device_id=\(self.DeviceId!)&device_mac_address=mac&brand_name=Apple&host_name=app&display_name=apple&serial_number=1234&version=\(kAppName)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"]) { (responseDict, error,isDone)in
            if error == nil
            {
                let dict = responseDict as! NSDictionary
                if dict.object(forKey: "statusCode") != nil
                {
                    let status = dict.object(forKey: "statusCode") as! Int
                    if status == 200
                    {
                        let result = dict.object(forKey: "result") as! NSDictionary
                        self.uuid = result.value(forKey: "uuid") as! String
                        self.DeviceId = result.value(forKey: "device_id") as! String
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                //                                    if !((dict["pair_devce"]! as String).contains("active"))
                //                                    {
                //    let rootView = storyBoard.instantiateViewController(withIdentifier: "Code") as! CodeViewController
                let rootView = storyBoard.instantiateViewController(withIdentifier: "PPCode") as! PPCodeViewController
                rootView.uuid = self.uuid
                rootView.deviceId = self.DeviceId
                rootView.code = result.value(forKey: "code") as! String
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                viewcontroller.navigationController?.pushViewController(rootView, animated: true)
                // }
            }
        }
    }
  }
}
    
    @objc func getstarRating(assetId:String,UserId:String)
    {
   //     let parameters = ["getAssetRating":["assetId":assetId,"userId":gUser_ID as AnyObject]]
        var rating:Float = 0.0
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MainViewController
   //     let url = "http://34.200.110.244:9000/getAssetRating"
     /*   ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]){
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict
              
                let arr = JSON as! NSArray
                let dict = arr.firstObject as! NSDictionary
            
                if (dict["averagerating"] is NSNull)
                {
                    rating = 0.0
                }
                if (dict["averagerating"] is Int)
                {
                    rating = Float(dict["averagerating"] as! Float)
                }
                
                viewcontroller.cosmosview.rating = Double(rating)
                viewcontroller.ratingLbl.text = "(\(rating)/\(viewcontroller.cosmosview.settings.totalStars))"
            }
            else
            {
                viewcontroller.cosmosview.rating = Double(rating)
                viewcontroller.ratingLbl.text = "\(rating) / \(viewcontroller.cosmosview.settings.totalStars)"
                print(error?.localizedDescription)
            }
        
    
    }*/
        let url = kgetratingUrl + "appname=\(kAppName)&token=\(self.UserId!)&assetId=\(assetId)"
        ApiManager.sharedManager.postDataWithJsonLambda(url: url, parameters: ["appname":"\(kAppName)"])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["statusCode"] != nil
                {
                  let status = JSON["statusCode"] as! Int
                  if status == 200
                  {
                     let result = JSON["result"] as! NSArray
                     let dict = result.firstObject as! NSDictionary
                    if (dict["averagerating"] is NSNull)
                    {
                        rating = 0.0
                    }
                    if (dict["averagerating"] is Float)
                    {
                        rating = Float(dict["averagerating"] as! Float)
                    }
                    
                    viewcontroller.cosmosview.rating = Double(rating)
                    let rate = String(format: "%0.1f", rating)
                    viewcontroller.ratingLbl.text = "(\(rate)/\(viewcontroller.cosmosview.settings.totalStars))"

                  }
                  else
                  {
                    viewcontroller.cosmosview.rating = Double(rating)
                    viewcontroller.ratingLbl.text = "\(rating) / \(viewcontroller.cosmosview.settings.totalStars)"
                  }
                }
                else
                {
                    viewcontroller.cosmosview.rating = Double(rating)
                    viewcontroller.ratingLbl.text = "\(rating) / \(viewcontroller.cosmosview.settings.totalStars)"

                }
            }
            else
            {
                //print(error?.localizedDescription ?? "json error in getrating")
                flixlog(str:error?.localizedDescription ?? "json error in getrating" )
                viewcontroller.cosmosview.rating = Double(rating)
                viewcontroller.ratingLbl.text = "\(rating) / \(viewcontroller.cosmosview.settings.totalStars)"
            }
        }
    }
    
    // Categories Controller
    @objc func gotoCategories()
    {
        let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
        let CategoriesPage = storyBoard.instantiateViewController(withIdentifier: "Categories") as! CategoriesViewController
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MainViewController
        CategoriesPage.CategoryCarousalName = viewcontroller.CategoryCarousalName
        CategoriesPage.CarousalData = viewcontroller.CarousalData
        CategoriesPage.UserId = viewcontroller.userId
        CategoriesPage.uuid = viewcontroller.uuid
        CategoriesPage.deviceId = viewcontroller.deviceId
        CategoriesPage.MyListthumb = viewcontroller.myList
        CategoriesPage.PhotoDict = viewcontroller.PhotoDict
        CategoriesPage.storeProdData = viewcontroller.storeProdData
        CategoriesPage.Donatedict = viewcontroller.DonateDict
        CategoriesPage.categoryDelegate = viewcontroller
        CategoriesPage.userData = self.userData
        CategoriesPage.CarousalData1 = viewcontroller.CarousalData1
        viewcontroller.navigationController?.pushViewController(CategoriesPage, animated: true)
    }
    
}
